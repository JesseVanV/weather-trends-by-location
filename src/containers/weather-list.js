import React, { Component } from 'react';
import { connect } from 'react-redux';
import Chart from '../components/chart';
import GoogleMap from '../components/google-map';

class WeatherList extends Component {

  renderWeather(citydata) {

    const name = citydata.city.name;
    const temps = citydata.list.map(weather => weather.main.temp);
    const pressure = citydata.list.map(weather => weather.main.pressure);
    const humidity = citydata.list.map(weather => weather.main.humidity);
    const { lat, lon } = citydata.city.coord;
      return (
        <tr key={ name }>
          <td><GoogleMap lat={lat} lon={lon} /></td>
          <td><Chart data={temps} color='red' units='&deg;F' /></td>
          <td><Chart data={pressure} color='green' units='hPa' /></td>
          <td><Chart data={humidity} color='blue' units='%' /></td>
        </tr>
      );
  }

  render() {
    return (
      <table className="table table-hover">
      <thead>
      <tr>
        <th>City</th>
        <th>Temperature (&deg;)</th>
        <th>Pressure (hpA) </th>
        <th>Humidity (%)</th>
      </tr>
      </thead>
      <tbody>
       { this.props.weather.map( this.renderWeather )}
      </tbody>
      </table>
    );
  }
};

function connectStatetoProps({ weather }) {  // ES6 to grab state.weather
  return { weather };
}

export default connect(connectStatetoProps)(WeatherList);  //export weatherList connected to state
