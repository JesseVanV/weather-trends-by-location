import React, { Component } from 'react';

class GoogleMap extends Component {

  componentDidMount() {  // basic component method, always loads
    new google.maps.Map(this.refs.map, {  // give html node to google maps
      zoom: 12,
      center: {
        lat: this.props.lat,
        lng: this.props.lon
      }
    })
  };

  render() {
    return <div ref='map'></div>;
  }
}

export default GoogleMap;
