import _ from 'lodash';
import React from 'react';
import { Sparklines, SparklinesLine, SparklinesReferenceLine } from 'react-sparklines';


function average(data) {
  const thisData =  (_.sum(data)/data.length);
  return thisData.toFixed(2);
}

export default (props) => {
  return (
  <div>
      <Sparklines data={ props.data } width={150} height={100}>
      <SparklinesLine color={ props.color } />
      <SparklinesReferenceLine type='avg' />
    </Sparklines>
    <div>Average: {average(props.data)}{ props.units }</div>
  </div>
  )
}
