import Axios from 'axios';

const API_KEY = 'fac18c0528d882070290fde603510e7a';
const MAIN_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${ API_KEY }&units=imperial`

export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather(cityName) {

  const url = `${ MAIN_URL }&q=${ cityName },us`;
  const request = Axios.get(url);  // 1. ajax request, returns a promise

  return {
    type: FETCH_WEATHER,
    payload: request  // 2. returning the ajax promise here as payload
  }
}
